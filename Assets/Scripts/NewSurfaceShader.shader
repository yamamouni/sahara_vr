﻿
Shader "Custom/NewSurfaceShader"{
    Properties{
      _MainTex("Texture", 2D) = "surface" {}
      _MainTex1("Texture1", 2D) = "surface1" {}
      _MainTex2("Texture2", 2D) = "surface2" {}
      _MainTex3("Texture3", 2D) = "surface3" {}      
      _MainTex4("Texture4", 2D) = "surface4" {}
      _MainTex5("Texture5", 2D) = "surface5" {}
      _MainTex6("Texture6", 2D) = "surface6" {}

      _Amount("Extrusion Amount", Range(-0.0001,0.0001)) = 0

    }
        SubShader{

          Tags { "RenderType" = "Opaque" }
          CGPROGRAM
          #pragma surface surf Lambert vertex:vert



          struct Input {
              float3 worldPos;
              float2 uv_MainTex;

          };
          sampler2D _MainTex;
          sampler2D _MainTex1;
          sampler2D _MainTex2;
          sampler2D _MainTex3;
          sampler2D _MainTex4;
          sampler2D _MainTex5;
          sampler2D _MainTex6;
          float _Amount;

          
          void vert(inout appdata_full v) {
              float3 newNormal = normalize(v.vertex.y);
              if (v.vertex.y < 0.5 && v.vertex.y > 0) {
                  v.vertex.y = (newNormal/2) - 3*abs(cos((_Time.w + (v.vertex.z) * 50)));

              }
          }



          void surf(Input IN, inout SurfaceOutput o) {

            float3 worlduv = (IN.worldPos/25);
            if ( IN.worldPos.y < 2) {
                //o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
                o.Albedo = tex2D(_MainTex, worlduv).rgb;
                


            }
            else if (IN.worldPos.y > 2 && IN.worldPos.y < 4) {
                o.Albedo = tex2D(_MainTex1, worlduv).rgb;
                
            }
            else if (IN.worldPos.y > 4 && IN.worldPos.y < 6) {
                o.Albedo = tex2D(_MainTex2, worlduv).rgb;
            }
            else if (IN.worldPos.y > 6 && IN.worldPos.y < 8) {
                o.Albedo = tex2D(_MainTex3, worlduv).rgb;
            }
            else if (IN.worldPos.y > 8 && IN.worldPos.y < 10) {
                o.Albedo = tex2D(_MainTex4, worlduv).rgb;
            }
            else if (IN.worldPos.y > 10 && IN.worldPos.y < 12) {
                o.Albedo = tex2D(_MainTex5, worlduv).rgb;
            }
            else if (IN.worldPos.y > 12) {

                o.Albedo = tex2D(_MainTex6, worlduv).rgb;
            }


            
          }
          ENDCG
    }
}




