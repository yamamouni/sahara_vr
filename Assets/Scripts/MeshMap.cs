using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MeshFilter))]
public class MeshMap : MonoBehaviour
{
    public int seed;
    public float scale;
    public int octaves;
    public float persistance;
    public float lacunarity;
    public Vector2 offset;
    public float meshHeightMultiplier;
    public AnimationCurve meshHeightCurve;

    //****************
    [Range(1.5f, 5f)]
    public float radius = 2f;

    [Range(0.5f, 5000f)]
    public float deformationStrength = 20f;

    private Mesh mesh;

    private Vector3[] verticies, modifiedVerts;

    // Start is called before the first frame update
    void Start()
    {
        float[,] heightMap = Noise.GenerateNoiseMap(241, 241, seed, scale, octaves, persistance, lacunarity, offset);
        GetComponent<MeshFilter>().mesh = MeshGenerator.GenerateTerrainMesh(heightMap,meshHeightMultiplier, meshHeightCurve, 0).CreateMesh();

        mesh = GetComponentInChildren<MeshFilter>().mesh;
        verticies = mesh.vertices;
        modifiedVerts = mesh.vertices;
    }

    // Update is called once per frame
    void Update()
    {
        
        RaycastHit hit;
        //Ray ray = myCam.ScreenPointToRay(Input.mousePosition);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);


        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            for (int v = 0; v < modifiedVerts.Length; v++)
            {
                Vector3 distance = modifiedVerts[v] - hit.point;

                float smoothingFactor = 2f;
                float force = deformationStrength / (1f + hit.point.sqrMagnitude);

                if (distance.sqrMagnitude < radius)
                {
                    if (Input.GetMouseButton(0))
                    {
                        modifiedVerts[v] = modifiedVerts[v] + (Vector3.up * force) / smoothingFactor;
                    }
                    if (Input.GetMouseButton(1))
                    {
                        modifiedVerts[v] = modifiedVerts[v] + (Vector3.down * force) / smoothingFactor;
                    }
                }
            }
        }
        RecalculateMesh();
    }
    
    void RecalculateMesh()
    {
        mesh.vertices = modifiedVerts;
        mesh.RecalculateNormals();
        GetComponentInChildren<MeshCollider>().sharedMesh = mesh;

        
    }
}
